# Gestión Condomininos
Aplicación web para gestión de condominios. 
## Instalación

### Requerimientos
* Linux/Windows
* PHP 7.0
* Composer version 1.7.*
* nodejs v8.*
* MySQL/mariaDB v10.3.*
* phpMyAdmin
* Laravel 5

### Dependencias

Si se tiene algun problema con las dependencias. Ejecutar:

```composer install```

# Pasos para instalar el Sistema desde Cero

Crear una base de datos en MySQL/mariaDB llamada gestion-condominios.

Modificar en archivo .env las siguientes variables globales

```
DB_DATABASE=gestion_condominios
DB_USERNAME=(nombre de usuario de la BD)
DB_PASSWORD=(password)
```

## Mailtrap

El sistema hace uso de la plataforma Mailtrap como servidor de correos entrantes y salientes. 
Se debe estar registrado en mailtrap.io 
En el archivo .env se deben colocar las credenciales de mailtrap 

```
MAIL_USERNAME=XXXXXXXXXXX
MAIL_PASSWORD=XXXXXXXXXXXXXXX
```

Ejecutar las migraciones con los seeders

```php artisan migrate --seed```

Se crearán las tablas en la base de datos (algunas con datos precargados).


Una vez hecho esto. Iniciar el servidor de laravel
 
```php artisan serve```

Entrar a la aplicación a través de la URL

```http://127.0.0.1:8000/```